#!/bin/bash
set -euo pipefail

: "${VARNISH_CONF_DIR:=/etc/varnish}"
: "${VARNISH_TEMPLATE_DIR:=/etc/varnish/templated}"
: "${VARNISH_CONF_FILE:=default.vcl}"
: "${VARNISH_LISTEN_ADDR:=:6081}"
: "${VARNISH_STORAGE_SPEC:=default,256M}"

cmd=''
args=()

template_configfiles() {
    if ! [ -d "$VARNISH_TEMPLATE_DIR" ]; then
        return
    fi

    for file in "$VARNISH_TEMPLATE_DIR"/*.tmpl; do
        targetfile="$VARNISH_CONF_DIR/$(basename "$file" .tmpl)"
        if [ -f "$file" ]; then
            echo "templating $file"
            envsubst < "$file" > "$targetfile"
        fi
    done

    echo "copying files"
    cp "$VARNISH_TEMPLATE_DIR"/* "$VARNISH_CONF_DIR"
}

if [ $# -eq 0 ]; then
    template_configfiles
    cmd='/usr/sbin/varnishd'
    args+=('-f' "$VARNISH_CONF_FILE"
            '-a' "$VARNISH_LISTEN_ADDR"
            '-s' "$VARNISH_STORAGE_SPEC"
            '-F')
else
    case "$1" in
        prometheus_exporter|prometheus_varnish_exporter)
            shift
            cmd=/usr/local/sbin/prometheus_varnish_exporter
            args=("$@")
            ;;
        ncsa|varnishncsa)
            shift
            cmd=/usr/sbin/varnishncsa
            args+=("$@")
            ;;
        log|varnishlog)
            shift
            cmd=/usr/sbin/varnishlog
            args+=("$@")
            ;;
        varnishd)
            shift
            ;;& # fallthrough
        * )
            template_configfiles
            cmd=/usr/sbin/varnishd
            args+=('-F')
            args+=("$@")
            ;;
    esac
fi

exec "$cmd" "${args[@]}"
