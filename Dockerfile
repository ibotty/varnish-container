FROM centos:7
LABEL MAINTAINER=tob@butter.sh \
      io.k8s.description="Varnish Cache" \
      io.k8s.display-name="varnish cache" \
      io.openshift.expose-services="6081:http" \
      io.openshift.tags="cache,varnish"

ENV VARNISH_INIT_VCL=/etc/varnish/default.vcl \
    VARNISH_EXPORTER_VERSION=1.5.2 \
    VARNISH_EXPORTER_SHA256SUM=3ee8c4c59aea1c341b9f4750950f24c8e6d9670ae39ed44af273f08ea318ede8

EXPOSE 6081/tcp

VOLUME /var/lib/varnish

# Install system utils & Gogs runtime dependencies
RUN repo=https://copr.fedorainfracloud.org/coprs/ingvar/varnish63/repo/epel-7/ingvar-varnish63-epel-7.repo  \
 && varnish_exporter_url=https://github.com/jonnenauha/prometheus_varnish_exporter/releases/download/${VARNISH_EXPORTER_VERSION}/prometheus_varnish_exporter-${VARNISH_EXPORTER_VERSION}.linux-amd64.tar.gz \
 && curl -sSLo prometheus_varnish_exporter.tar.gz "$varnish_exporter_url" \
 && sha256sum prometheus_varnish_exporter.tar.gz | while read checksum _f; do \
    [ "$checksum" = "$VARNISH_EXPORTER_SHA256SUM" ] ; done \
 && tar xzf prometheus_varnish_exporter.tar.gz "prometheus_varnish_exporter-${VARNISH_EXPORTER_VERSION}.linux-amd64/prometheus_varnish_exporter" \
 && mv prometheus_varnish_exporter-*/prometheus_varnish_exporter /usr/local/sbin \
 && rm -r prometheus_varnish_exporter* \
 && curl -sSLo /etc/yum.repos.d/ingvar-varnish63-epel-7.repo "$repo" \
 && yum install -y epel-release \
 && yum install -y --setopt=tsflags=nodocs gettext varnish varnish-modules \
 && test "$(id -u varnish)" -eq 999 \
 && chmod -R g+w /var/lib/varnish /etc/varnish

ADD entrypoint.sh /usr/libexec/varnish-container/entrypoint

USER 999
ENTRYPOINT /usr/libexec/varnish-container/entrypoint
